package com.example.recyclerview

import android.annotation.SuppressLint
import android.content.Intent
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class DetailActivity : AppCompatActivity() {
    private lateinit var mediaPlayer: MediaPlayer
    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val receivedData=if (Build.VERSION.SDK_INT>=33){
            @Suppress("DEPRECATION")
            intent.getParcelableExtra<video>("shadow")
        }else{
            @Suppress("DEPRECATION")
            intent.getParcelableExtra<video>("shadow")
        }
        val gambar:ImageView=findViewById(R.id.img_gambar)
        val judul:TextView=findViewById(R.id.tv_Judul)
        val description:TextView=findViewById(R.id.tv_description)
        val playButton:ImageView=findViewById(R.id.tombol_play)
        val audioButton:Button=findViewById(R.id.audio_button)
        if (receivedData!=null){
            mediaPlayer = MediaPlayer.create(this, receivedData.audioId)
            audioButton.setOnClickListener {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause()
                    audioButton.setText("Play")
                }
                else {
                    mediaPlayer.start()
                    audioButton.setText("Pause")
                }
            }
            gambar.setImageResource(receivedData.gambar)
            judul.text=receivedData.judul
            description.text=receivedData.data_deskripsi
            playButton.setOnClickListener{
                val videoIntent= Intent(this,video_Activity::class.java)
                videoIntent.putExtra("videoId",receivedData.videoId)
                startActivity(videoIntent)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        // Release the MediaPlayer when the activity is destroyed
        if (mediaPlayer != null) {
            mediaPlayer.release()
        }
    }
}