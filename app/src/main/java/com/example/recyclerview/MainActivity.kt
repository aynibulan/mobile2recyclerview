package com.example.recyclerview

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private val list= ArrayList<video>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //gunanya untuk membuka sharedpreferences dengan nama "SESSION"
        val session = getSharedPreferences("session", Context.MODE_PRIVATE)
        //mengecek apakah user sudah login atau belum
        val isLogin = session.getBoolean("isLogin", false)
        //jika belum login, alihkan ke halaman login
        if (!isLogin) {
            val intent = Intent(this, Login::class.java)
            startActivity(intent)
        }
        setContentView(R.layout.activity_main)
        recyclerView=findViewById(R.id.rv_video)
        recyclerView.setHasFixedSize(true)
        list.addAll(getList())
        showRecyclerList()
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_logout, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.logout) {
            logout()
        }
        return  true
    }

    private fun logout() {
        val session = getSharedPreferences("session", Context.MODE_PRIVATE)
        val editor = session.edit()
        editor.putBoolean("isLogin", false)
        editor.apply()
        val intent = Intent(this, Login::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
    private fun getList():ArrayList<video>{
        val gambar=resources.obtainTypedArray(R.array.data_gambar)
        val dataName=resources.getStringArray(R.array.judul_video)
        val dataDescription=resources.getStringArray(R.array.data_dekripsi)
        val videoId=resources.obtainTypedArray(R.array.video_id)
        val audioId=resources.obtainTypedArray(R.array.audio_id)
        val listvideo=ArrayList<video>()
        for (i in dataName.indices){
            val video=video(gambar.getResourceId(i,-1),dataName[i],dataDescription[i],videoId.getResourceId(i,-1),audioId.getResourceId(i, -1) )
            listvideo.add(video)
        }
        return listvideo
    }
    private fun showRecyclerList(){
        recyclerView.layoutManager=LinearLayoutManager(this)
        val listadapter=ListAdapter(list)
        recyclerView.adapter=listadapter
    }

}